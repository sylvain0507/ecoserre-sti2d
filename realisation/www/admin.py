#!/usr/bin/env python3
# coding: utf-8

from flask import Flask, render_template, request, session, Response
import serial
import time
import os
import io
#import picamera

import sys
sys.path.append("../raspberry")
import readSerial as read
import writeSerial as write
import display_log as dsp

#----- configuration : à paramétrer -----
WIFI = False
IP   = "192.168.1.100"
PORT = 1234
COM  = "COM12"
BAUD = 9600
#----------------------------------------
DATA = "./static/data"

serialPort = serial.Serial(COM, BAUD)


app = Flask(__name__, template_folder='tpl')
app.secret_key = 'ecoserre'

def send_command(data: str) -> bool:
    """ envoi une commande à l'iot """
    if WIFI:
        return write.writeWifi(IP, PORT, data)
    else:
        return write.writeSerial(serialPort, data)

@app.route('/', methods=['GET', 'POST'])
def index():
    """ admin home page """
    #valeurs par défaut LED + trappe + ventilation
    if 'light_value' not in session:
        session['light_value'] = 'OFF'
    if 'hatch_value' not in session:
        session['hatch_value'] = 'OFF'
    if 'fan_value' not in session:
        session['fan_value'] = 'OFF'

    #commandes LED + trappe + ventilation
    if request.method == 'POST':
        if 'light' in request.form:
            session['light_value'] = 'ON' if session['light_value'] == 'OFF' else 'OFF'
            send_command("LED="+session['light_value'])
        if 'hatch' in request.form:
            session['hatch_value'] = 'ON' if session['hatch_value'] == 'OFF' else 'OFF'
            if session['hatch_value'] == 'ON':
                send_command("SERVO=0:45")
            else:
                send_command("SERVO=45:0")
        if 'fan' in request.form:
            session['fan_value'] = 'ON' if session['fan_value'] == 'OFF' else 'OFF'
            send_command("FAN="+session['fan_value'])

    #couleur des boutons LED + trappe + ventilation
    light_button = "is-danger" if session['light_value'] == "OFF" else "is-primary"
    hatch_button = "is-danger" if session['hatch_value'] == "OFF" else "is-primary"
    fan_button   = "is-danger" if session['fan_value']   == "OFF" else "is-primary"

    #lecture des données du port série
    # format : data = String(temperature) + ";" + String(humidity) + ";" + String(light) + ";" + String(pressure)
    data = read.read_wifi(IP, PORT) if WIFI else read.read_serial(serialPort)
    if data is not None:
        read.write_data(DATA, data)
        temperature, humidity, light, pressure = data
    else:
        temperature, humidity, light, pressure = [0, 1, 2, 3]

    return render_template("admin.html",
        light_button=light_button, light_value=session['light_value'],
        hatch_button=hatch_button, hatch_value=session['hatch_value'],
        fan_button=fan_button, fan_value=session['fan_value'],
        temperature=temperature, humidity=humidity, light=light, pressure=pressure)

def generate_frames():
    """ capture vidéo """
    with picamera.PiCamera() as camera:
        camera.resolution = (640, 480)  # Résolution de la vidéo
        camera.framerate  = 24          # Images par seconde
        #camera.start_preview(fullscreen=False, window=(0, 0, 640, 480))
        time.sleep(2)  # Attente de 2 secondes pour laisser la caméra s'ajuster

        stream = io.BytesIO()
        for _ in camera.capture_continuous(stream, 'jpeg', use_video_port=True):
            stream.seek(0)
            yield b'--frame\r\nContent-Type: image/jpeg\r\n\r\n' + stream.read() + b'\r\n'
            stream.seek(0)
            stream.truncate()

@app.route('/video_feed')
def video_feed():
    return Response(generate_frames(), mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/camera', methods=['GET', 'POST'])
def camera():
    """ affiche la caméra """
    return render_template("camera.html")

@app.route('/logs', methods=['GET', 'POST'])
def logs():
    """ affiche les journeaux """
    def delete_old_files(directory : str, max_age : int):
        """ suppression des fichiers trop anciens """
        current_time = time.time()
        for filename in os.listdir(directory):
            file_path = os.path.join(directory, filename)
            if os.path.isfile(file_path):
                file_age = current_time - os.path.getmtime(file_path)
                if file_age > max_age:
                    os.remove(file_path)
                    print("Deleted file:", file_path)

    def get_files(folder_path : str, extension : str) -> list:
        """ renvoie les fichiers d'un dossier avec une extension donnée """
        files = []
        for file in os.listdir(folder_path):
            if file.endswith(extension):
                files.append(file)
        return files

    images = []
    date   = request.form.get('date', default="")
    if date != "":
        #delete_old_files(DATA, 3600)
        images = dsp.display_logs(date, DATA)

    return render_template("logs.html",
        options=get_files(DATA, "csv"),
        images=images, nocache=time.time())

if __name__ == "__main__":
    app.run(debug=True)
