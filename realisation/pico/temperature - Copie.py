#! /usr/bin/python

import smbus2
import time

ADDRESS    = 0x48
RESOLUTION = 0x80

REG_CONFIG   = 0x03 
REG_STATUS   = 0x02
REG_LSB_READ = 0x01
REG_MSB_READ = 0x00


class ADT7410:
    """Capteur de température ADT7410"""
    def __init__(self, address: int, bus: int = 1):
        self.address = address
        self.bus     = smbus2.SMBus(bus)

    def set_resolution(self, resolution: int) -> bool:
        try:
            self.bus.write_byte_data(self.address, REG_CONFIG, resolution)
            return True
        except OSError:
            return False

    def read_temperature(self) -> float:
        try:
            while (self.bus.read_byte_data(self.address, REG_STATUS) & 0x80) == 0:
                time.sleep(0.01)  # Attendre 10 ms avant chaque nouvelle tentative

            lsb = self.bus.read_byte_data(self.address, REG_LSB_READ)
            msb = self.bus.read_byte_data(self.address, REG_MSB_READ)
            result = (msb << 5) | (lsb >> 3)

            return result / 16
        except OSError:
            return None


if __name__ == "__main__":
    adt = ADT7410(ADDRESS)
    if not adt.set_resolution(RESOLUTION):
        print("Erreur de communication I2C lors de la configuration du capteur:")

    try:
        while True:
            temperature = adt.read_temperature()
            if temperature is None:
                print("Erreur de communication I2C lors de la lecture de la température:")
            else:
                print("Température : {} °C".format(temperature))
            time.sleep(2)  # Attendre 2 secondes entre chaque lecture de température
    except KeyboardInterrupt:
        # Arrêter la boucle principale lorsque l'utilisateur appuie sur Ctrl+C
        print("Arrêt du programme")
