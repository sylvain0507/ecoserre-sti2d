import machine
import time
import network
import socket

ssid       = "SSID"         # Service Set Identifier
password   = "Password"
ip_address = "ipAddress"

def wifi_init():
    # Connexion au réseau Wi-Fi
    sta_if = network.WLAN(network.STA_IF)   # Station interface
    sta_if.active(True)
    sta_if.connect(ssid, password)
    while not sta_if.isconnected():
        print("Connexion au Wi-Fi...")
        time.sleep(1)
    print("Connecté au réseau Wi-Fi")

def wifi_read():
    data   = ""

    client = network.WLAN(network.STA_IF)
    if client.isconnected():
        address = socket.getaddrinfo(ip_address, 80)[0][-1]
        sock    = socket.socket()
        sock.connect(address)
        sock.settimeout(1)  # Définir un délai d'attente de 1 seconde pour les opérations de réception
        
        eot = False
        while not eot:
            try:
                received_data = sock.recv(1)
                if received_data != b'\n':  # Attention à l'encodage !
                    data += received_data.decode()
                else:
                    eot = True
            except socket.timeout:
                eot = True
        sock.close()

    return data

def wifi_write(data):
    client = network.WLAN(network.STA_IF)
    if client.isconnected():
        address = socket.getaddrinfo(ip_address, 80)[0][-1]
        sock    = socket.socket()
        sock.connect(address)
        sock.sendall("GET /update?data=" + data + " HTTP/1.1\r\n"
                  "Host: " + ip_address + "\r\n"
                  "Connection: close\r\n\r\n")
        sock.close()
        return True
    else:
        return False

def setup():
    wifi_init()

def loop():
    wifi_write("Hello")
    data = read_wifi()
    if data != "":
        print(data)
    time.sleep(4)  # Attendre 4s

if __name__ == "__main__":
    setup()
    while True:
        loop()
