import machine
import time

def setup():
    i2c = machine.I2C(0)
    print("Scanning for I2C devices")

    deviceCount = 0
    for address in range(1, 128):
        try:
            i2c.readfrom(address, 1)
            print("\n Found I2C device at address 0x" + "{:02x}".format(address))
            deviceCount += 1
        except OSError:
            print(".", end="")
        time.sleep_ms(5)

    if deviceCount == 0:
        print("\n No I2C devices found.")
    else:
        print("\n Scanning finished.")

def loop():
    # Ne rien faire dans la boucle principale
    pass

if __name__ == "__main__":
    setup()
    while True:
        loop()
