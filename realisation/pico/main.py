import wifi
import led, servo
import temperature, humidity, light, pressure

import machine
import time
import network

DEBUG = True  # Mode verbeux
WIFI  = True  # Utilisation du Wi-Fi
INTERRUPT_PIN = 2  # Broche utilisée pour l'interruption

def processSerialData():
    if WIFI:
        data = wifi.wifi_read()
    else:
        data = machine.Serial.read().decode().strip()

    # Traite les données
    if data.startswith("LED="):
        # cmde : LED=[ON|OFF]
        value = data[4:]
        if value == "ON":
            led.led_on()
        elif value == "OFF":
            led.led_off()
        else:
            print("Erreur : Commande LED incorrecte")  # Envoie une réponse à la Raspberry
    elif data.startswith("SERVO="):
        # cmde : SERVO=position_départ:position_fin
        value = data[6:]
        separatorIndex = value.find(':')
        if separatorIndex != -1:
            value1 = value[:separatorIndex]
            value2 = value[separatorIndex + 1:]
            servo.rotate(int(value1), int(value2))
        else:
            print("Erreur : Syntaxe SERVO incorrecte")  # Envoie une réponse à la Raspberry
    else:
        print("Erreur : Commande non reconnue")  # Envoie une réponse à la Raspberry

def setup():
    pressure.adt7410_init()
    led.led_init()
    servo.servo_init()
    if WIFI:
        wifi.wifi_init()

    machine.Pin(INTERRUPT_PIN, machine.Pin.IN, machine.Pin.PULL_UP)
    machine.Pin(INTERRUPT_PIN).irq(trigger=machine.Pin.IRQ_FALLING, handler=processSerialData)

def loop():
    try:
        temp_value     = temperature.read_temperature()
        humidity_value = humidity.read_humidity()
        light_value    = light.read_light()
        pressure_value = pressure.read_pressure()

        if DEBUG:
            print("Temperature: " + str(temp_value) + " °C")
            print("Humidity: " + str(humidity_value) + "%")
            print("Light: " + str(light_value) + "lx")
            print("Pressure: " + str(pressure_value) + "hPa")

        data = str(temp_value) + ";" + str(humidity_value) + ";" + str(light_value) + ";" + str(pressure_value)
        if WIFI:
            if not wifi.wifi_write(data):
                print("Erreur : Connexion au client Wi-Fi impossible")
        else:
            print(data)
    except Exception as e:
        print("Erreur : " + str(e))

    time.sleep(5)  # Attendre 5s avant la prochaine lecture

if __name__ == "__main__":
    setup()
    while True:
        loop()
