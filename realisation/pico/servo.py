import machine
import utime

# Déclaration de la broche de commande du servo-moteur
servoPin = 6

# Création d'une instance de l'objet Servo
servo = machine.PWM(machine.Pin(servoPin))

def servo_init():
    servo.freq(50)  # Réglage de la fréquence du signal PWM du servo

def rotate(from_angle, to_angle):
    if from_angle < 0 or from_angle > 180 or to_angle < 0 or to_angle > 180:
        return

    if from_angle < to_angle:
        for angle in range(from_angle, to_angle + 1):
            servo.duty_ns(500 + angle * 2000 // 180)
            utime.sleep_ms(15)  # Attendre un court instant pour la rotation
    else:
        for angle in range(from_angle, to_angle - 1, -1):
            servo.duty_ns(500 + angle * 2000 // 180)
            utime.sleep_ms(15)  # Attendre un court instant pour la rotation

def setup():
    # Attache le servo à la broche spécifiée
    servo_init()

def loop():
    # Rotation du servo-moteur de 0° à 180°
    rotate(0, 180)

    # Attente de 1 seconde à la position maximale (180°)
    utime.sleep(0.5)

    # Rotation du servo-moteur de 180° à 0°
    rotate(180, 0)

    # Attente de 1 seconde à la position minimale (0°)
    utime.sleep(0.5)

if __name__ == "__main__":
    setup()
    while True:
        loop()
