import machine
import time

humidityPin = 25

def read_humidity():
    # HIH4000
    bits = 10
    
    rawValue = machine.ADC(humidityPin)
    rawValue = rawValue.read_u16()

    # Conversion en pourcentage d'humidité
    humidity = rawValue * 100 // (2**bits - 1)
    return humidity

def setup():
    pass

def loop():
    # Affichage de l'humidité
    humidity = read_humidity()
    print("Humidity: {}%\r\n".format(humidity))
    
    time.sleep(1)  # Attendre 1 seconde avant la prochaine lecture

if __name__ == "__main__":
    setup()
    while True:
        loop()
