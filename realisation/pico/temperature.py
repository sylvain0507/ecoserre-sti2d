import machine
import time

SDA_PIN = 4
SCL_PIN = 5

ADDRESS    = 0x48
RESOLUTION = 0x80

REG_CONFIG   = 0x03
REG_STATUS   = 0x02
REG_LSB_READ = 0x01
REG_MSB_READ = 0x00


class ADT7410:
    """Capteur de température ADT7410"""
    def __init__(self, address: int, bus: int = 0):
        self.address = address
        self.bus     = machine.I2C(bus, scl=machine.Pin(SCL_PIN), sda=machine.Pin(SDA_PIN))

    def set_resolution(self, resolution: int) -> bool:
        try:
            self.bus.writeto(self.address, bytes([REG_CONFIG, resolution]))
            return True
        except OSError:
            return False

    def read_temperature(self) -> float:
        try:
            while (self.bus.readfrom(self.address, 1, REG_STATUS)[0] & 0x80) == 0:
                time.sleep(0.01)  # Attendre 10 ms avant chaque nouvelle tentative

            lsb = self.bus.readfrom(self.address, 1, REG_LSB_READ)[0]
            msb = self.bus.readfrom(self.address, 1, REG_MSB_READ)[0]
            result = (msb << 5) | (lsb >> 3)

            return result / 16
        except OSError:
            return None


if __name__ == "__main__":
    adt = ADT7410(ADDRESS)
    if not adt.set_resolution(RESOLUTION):
        print("Erreur de communication I2C lors de la configuration du capteur")

    try:
        while True:
            temperature = adt.read_temperature()
            if temperature is None:
                print("Erreur de communication I2C lors de la lecture de la température")
            else:
                print("Température : {} °C".format(temperature))
            time.sleep(2)  # Attendre 2s entre chaque lecture de température
    except KeyboardInterrupt:
        # Arrêter la boucle principale lorsque l'utilisateur appuie sur Ctrl+C
        print("Arrêt du programme")
