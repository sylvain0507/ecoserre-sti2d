import machine
import math
import time

ldrPin = 26

def read_light():
    # ln(lx) = 1.55e-11*N^4 - 2.86e-9*N^3 - 1.60e-5*N^2 + 1.78e-2*N
    N = machine.ADC(ldrPin)
    N = N.read_u16()
    lx = 1.55e-11 * math.pow(N, 4) - 2.86e-9 * math.pow(N, 3) - 1.60e-5 * math.pow(N, 2) + 1.78e-2 * N
    return math.exp(lx)

def setup():
    pass

def loop():
    # Affichage de la luminosité
    light = read_light()
    print("Light: {:.1f} lx\r\n".format(light))
    
    time.sleep(1)  # Attendre 1 seconde avant la prochaine lecture

if __name__ == "__main__":
    setup()
    while True:
        loop()
