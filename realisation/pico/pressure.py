import machine
import time

pressurePin = 26

def read_pressure():
    # Velleman MM103 Analog Pressure (15kPa - 115kPa)
    bits = 10

    Pmin = 15.0  # en kPa
    Pmax = 115.0  # en kPa

    # Lecture de la tension analogique
    rawValue = machine.ADC(pressurePin)
    rawValue = rawValue.read_u16()

    # Calcul de la pression en kPa
    pressure = Pmin + (rawValue * (Pmax - Pmin) / (2 ** bits - 1))

    # Renvoie la valeur en hPa
    return pressure * 10

def setup():
    pass

def loop():
    # Affichage de la pression
    pressure = read_pressure()
    print("Pressure: {:.1f} hPa\r\n".format(pressure))
    
    time.sleep(1)  # Attendre 1 seconde avant la prochaine lecture

if __name__ == "__main__":
    setup()
    while True:
        loop()
