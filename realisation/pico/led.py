import machine
import time

ledPin = 2

def led_init():
    pass  # Pas besoin d'initialisation pour la broche GPIO en MicroPython

def led_on():
    machine.Pin(ledPin, machine.Pin.OUT).on()

def led_off():
    machine.Pin(ledPin, machine.Pin.OUT).off()

def setup():
    led_init()

def loop():
    for i in range(4):
        led_on()
        time.sleep(1)  # Attendre 1 seconde
        led_off()
        time.sleep(1)  # Attendre 1 seconde

if __name__ == "__main__":
    setup()
    while True:
        loop()
