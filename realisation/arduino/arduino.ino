const bool DEBUG = false;      // mode verbeux
const bool WIFI  = false;      // utilisation du wifi

unsigned long previousTime = 0;
const unsigned long interval = 5000;  // Intervalle de 5 secondes

void processSerialData(void) 
{
  String data = WIFI
	  ? wifi_read()
	  : Serial.readStringUntil('\n') ;  // Lit la chaîne de caractères reçue jusqu'au caractère de nouvelle ligne

  // Traite les données
  if ( DEBUG )
    Serial.println(data);
  if ( data.startsWith("LED=") ) {
      // cmde : LED=[ON|OFF]
      String value = data.substring(4);  //----- Extrait la valeur après le préfixe "LED="
      if ( value == "ON" )
        led_on();
      else if ( value == "OFF" )
        led_off();
      else if ( DEBUG )
        Serial.println("Erreur commande de la LED");
      }
  else if ( data.startsWith("FAN=") ) {
      // cmde : FAN=[ON|OFF]
      String value = data.substring(4);  //----- Extrait la valeur après le préfixe "FAN="
      if ( value == "ON" )
        fan_on();
      else if ( value == "OFF" )
        fan_off();
      else if ( DEBUG )
        Serial.println("Erreur commande du ventilateur");
      }
    else if ( data.startsWith("SERVO=") ) {
      // cmde : SERVO=position_départ:position_fin
      String value = data.substring(6);  //----- Extrait la valeur après le préfixe "SERVO="
      int    separatorIndex = value.indexOf(':');  // Trouve l'index du séparateur ":"
      if ( separatorIndex != -1 ) {
        String value1 = value.substring(0, separatorIndex);  // Extrait la première valeur après le préfixe "SERVO="
        String value2 = value.substring(separatorIndex + 1);  // Extrait la deuxième valeur après le séparateur ":"
        
        rotate( value1.toInt(), value2.toInt() );
        }
      else if ( DEBUG )
          Serial.println("Erreur syntaxe SERVO");
      }
   else if ( DEBUG )
    Serial.println("Erreur commande");  // Envoie une réponse à la Raspberry
}

#define __main__
void setup(void) 
{
  Serial.begin(9600);
  
  adt7410_init(); // Démarre la communication I2C
  if ( DEBUG )
    Serial.println("init ADT7410... ok");
  led_init();
  if ( DEBUG )
    Serial.println("init LED... ok");
  fan_init();
  if ( DEBUG )
    Serial.println("init FAN... ok");
  servo_init();
  if ( DEBUG )
    Serial.println("init servo... ok");
  if ( WIFI )
    wifi_init();
}

void loop(void) 
{
  unsigned long currentTime = millis();

  // Lecture des données série si l'intervalle de temps est écoulé
  if (currentTime - previousTime < interval) {
    if ( Serial.available() )
      processSerialData();
    }
  else {
    // le délai est écoulé
    previousTime = currentTime;
    
    // Lecture des capteurs
    float temperature = read_temperature();
    int humidity      = read_humidity();
    int light         = read_light();
    int pressure      = read_pressure();
    
    // Affichage des valeurs des capteurs
    if ( DEBUG ) {
      Serial.println("Temperature: " + String(temperature) + " °C");
      Serial.println("Humidity: " + String(humidity) + "%");
      Serial.println("Light: " + String(light) + "lx");
      Serial.println("Pressure: " + String(pressure) + "hPa");
      }
  
    String data = String(temperature) + ";" + String(humidity) + ";" + String(light) + ";" + String(pressure);
    if ( WIFI ) {
      if ( !wifi_write(data) && DEBUG )
        Serial.println("Erreur connexion client wifi");
      }
    else
      // Envoi des valeurs des capteurs via la communication série
      Serial.print(data + "\n");
    }
}
