const int ledPin = 2;

void led_init(void)
{
  pinMode(ledPin, OUTPUT);
}

void led_on(void)
{
  digitalWrite(ledPin, HIGH);
}

void led_off(void)
{
  digitalWrite(ledPin, LOW);
}

#ifndef __main__
void setup(void) 
{
  led_init();
}

void loop(void) 
{
  for (int i = 0; i < 4; i++) {
    led_on();
    delay(1000); // Attendre 1 seconde
    led_off();
    delay(1000); // Attendre 1 seconde
    }
}
#endif
