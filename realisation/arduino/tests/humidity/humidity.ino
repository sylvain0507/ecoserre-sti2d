const int humidityPin = A3;

int read_humidity(void)
{
  // HIH4000
  const int   bits = 10;
  
  int rawValue = analogRead(humidityPin);

  // Conversion en pourcentage d'humidité
  return map(rawValue, 0, pow(2, bits) - 1, 0, 100);
}

#ifndef __main__
void setup(void) 
{
  Serial.begin(9600);
}

void loop(void) 
{
  // Affichage de l'humidité
  int humidity = read_humidity();
  Serial.println("Humidity: " + String(humidity) + "%");
  
  delay(1000); // Attendre 1 seconde avant la prochaine lecture
}
#endif
