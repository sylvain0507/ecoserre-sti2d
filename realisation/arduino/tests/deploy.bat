@echo off
set count=0

rem Parcours des sous-répertoires
for /r %%G in (*.ino) do (
	copy %%~fG .. /Y 1>NUL
	set /a count+=1
)

echo %count% Fichiers .ino copies dans le dossier parent.
pause
