void setup() {
  pinMode(LED_BUILTIN, OUTPUT); // Configure la broche de la LED en sortie
  Serial.begin(9600); // Initialise la communication série
}

void loop() {
  if (Serial.available()) { // Vérifie si des données sont disponibles sur le port série
    char data = Serial.read(); // Lit les données reçues
    
    // Faites quelque chose avec les données lues
    // Par exemple, allumez une LED si la lettre "H" est reçue
    if (data == 'H') {
      digitalWrite(LED_BUILTIN, HIGH); // Allume la LED
    } else {
      digitalWrite(LED_BUILTIN, LOW); // Éteint la LED
    }
  }
}
