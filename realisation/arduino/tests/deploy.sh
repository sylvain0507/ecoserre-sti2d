#!/bin/bash

count=0

# Parcours des sous-répertoires et copie des fichiers .ino
find . -name "*.ino" -type f -exec cp {} ../ \; >/dev/null 2>&1
count=$(find . -name "*.ino" -type f | wc -l)

echo "$count fichiers .ino copiés dans le dossier parent."

read -p "Appuyez sur une touche pour continuer..."

