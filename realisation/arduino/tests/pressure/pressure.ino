#include <Wire.h>

const int pressurePin = A2;

float read_pressure(void)
{
  // Velleman MM103 Analog Pressure (15kPa - 115kPa)
  const int   bits = 10;

  const float Pmin = 15;  // en kPa
  const float Pmax = 115;  // en kPa

  // Lecture de la tension analogique
  int rawValue = analogRead(pressurePin);
  
  // Calcul de la pression en kPa
  float pressure = Pmin + (rawValue * (Pmax - Pmin) / (pow(2, bits) - 1));

  // renvoie la valeur en hPa
  return pressure * 10;
}

#ifndef __main__
void setup(void) 
{
  Serial.begin(9600);
}

void loop(void) 
{
  // Affichage de la température
  float pressure = read_pressure();
  Serial.println("Pressure: " + String(pressure) + " hPa");
  
  delay(1000); // Attendre 1 seconde avant la prochaine lecture
}
#endif
