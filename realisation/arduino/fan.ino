const int fanPin = 3;

void fan_init(void)
{
  pinMode(fanPin, OUTPUT);
}

void fan_on(void)
{
  digitalWrite(fanPin, HIGH);
}

void fan_off(void)
{
  digitalWrite(fanPin, LOW);
}

#ifndef __main__
void setup(void) 
{
  fan_init();
}

void loop(void) 
{
  for (int i = 0; i < 2; i++) {
    fan_on();
    delay(2000); // Attendre 2s
    fan_off();
    delay(2000);
    }
}
#endif
