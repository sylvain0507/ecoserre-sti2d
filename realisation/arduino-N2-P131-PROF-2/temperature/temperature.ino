#include <Wire.h>

const int address      = 0x48;
const int reg_config   = 0x03;
const int reg_status   = 0x02;
const int reg_lsb_read = 0x01;
const int reg_msb_read = 0x00;
const int resolution   = 0x80;  // 16 bits

void adt7410_init(void) 
{
  Wire.begin();
  Wire.beginTransmission(address);
  Wire.write(reg_config);
  Wire.write(resolution);
  Wire.endTransmission();
}

// Fonction pour lire un registre du BME280
byte readRegister(const int reg) 
{
  const int data_length = 3;
  
  if ( Wire.requestFrom(address, data_length) != data_length )
    Serial.println("erreur lecture registre");

  byte data[data_length] = { 0, 0, 0 };
  int  i = 0;
  while ( Wire.available() && i < data_length )
        data[i++] = Wire.read();

  return (reg >= 0) && (reg < data_length)
    ? data[reg]
    : 0 ;
}

float read_temperature(void)
{ 
  // ADT7410
  while (  (readRegister(reg_status) & 0x80) == 1 ) {
    // Attendre que la mesure soit prête
    Serial.print(".");
    delay(5);
    }
    
  byte LSB = readRegister(reg_lsb_read);
  byte MSB = readRegister(reg_msb_read);  
  int  result = (MSB << 5) | (LSB >> 3);
  
  //if ( MSB && (1 << 7) )
  //  result -= 4096;
  return static_cast<float>(result) / 16;
}

#ifndef __main__
void setup(void) 
{
  Serial.begin(9600);
  
  adt7410_init();
}

void loop(void) 
{
  // Affichage de la température
  float temperature = read_temperature();
  Serial.println("Temperature: " + String(temperature) + " °C");
  
  delay(2000); // Attendre 2s avant la prochaine lecture (holding time)
}
#endif
