#include <WiFi.h>

const String ssid       = "SSID";
const String password   = "Password";
const String ip_address = "ipAddress";

void wifi_init(void)
{
  // Connexion au réseau Wi-Fi
  WiFi.begin(const_cast<char*>(ssid.c_str()), password.c_str());
  while ( WiFi.status() != WL_CONNECTED ) {
    delay(1000);
    Serial.println("Connexion au Wi-Fi...");
    }
  Serial.println("Connecté au réseau Wi-Fi");
}

String wifi_read(void)
{
  String data = "";
  unsigned long timeout = millis() + 1000; // Délai maximal d'attente (1 seconde)

  char c = 0;
  while ( Serial.available() && c != '\n' && millis() < timeout ) {
    c = Serial.read();
    if ( c != '\n' )
        data += c;
    }
  Serial.flush(); // Vide le buffer de l'objet Serial
  
  return data;
}

bool wifi_write(const String &data)
{
  WiFiClient client;
  if ( client.connect(const_cast<char*>(ip_address.c_str()), 80) ) {
    client.println("GET /update?data=" + data + " HTTP/1.1");
    client.println("Host: "+ip_address);
    client.println("Connection: close");
    client.println();
    client.stop();
    return true;
    }
  else
    return false;
}

#ifndef __main__
void setup(void) 
{
  Serial.begin(9600);
  wifi_init();
}

void loop(void) 
{
  wifi_write("Hello");
  
  delay(5000); // Attendre 5s
}
#endif
