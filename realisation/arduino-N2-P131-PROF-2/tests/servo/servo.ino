#include <Servo.h>

// Déclaration de la broche de commande du servo-moteur
const int servoPin = 6;

// Création d'une instance de l'objet Servo
Servo servo;

void servo_init(void)
{
  servo.attach(servoPin);
}

void rotate(const int from, const int to)
{
  if ( from < 0 || from > 180 || to < 0 || to > 180 )
    return;
    
  if ( from < to )
  	for (int angle = from; angle <= to; angle += 1) {
  		servo.write(angle);
      delay(15); // Attendre un court instant pour la rotation
      }
  else
    for (int angle = from; angle >= to; angle -= 1) {
      servo.write(angle);
      delay(15); // Attendre un court instant pour la rotation
      }
}

#ifndef __main__
void setup(void) 
{
  // Attache le servo à la broche spécifiée
  servo_init();
}

void loop(void) 
{
  // Rotation du servo-moteur de 0° à 180°
  rotate(0, 180);

  // Attente de 1 seconde à la position maximale (180°)
  delay(500);

  // Rotation du servo-moteur de 180° à 0°
  rotate(180, 0);

  // Attente de 1 seconde à la position minimale (0°)
  delay(500);
}
#endif
