#include <Wire.h>

void setup(void) 
{
  Wire.begin();

  Serial.begin(9600);
  while (!Serial);

  Serial.println("Scanning for I2C devices");

  byte address;
  int  deviceCount = 0;

  for(address = 1; address < 127; address++) {
    Wire.beginTransmission(address);
    if ( Wire.endTransmission() == 0 ) {
      Serial.print("\n Found I2C device at address 0x");
      if ( address < 16 )
        Serial.print("0");
      Serial.print(address, HEX);
      Serial.println("");
      deviceCount++;
      }
    else
      Serial.print(".");

    delay(5);
    }

  if ( deviceCount == 0 )
    Serial.println("\n No I2C devices found.");
  else
    Serial.println("\n Scanning finished.");
}

void loop(void) 
{
  // Ne rien faire dans la boucle principale
}
