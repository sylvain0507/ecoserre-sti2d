const int ldrPin = A0;

int read_light(void)
{
  // ln(lx) = 1,55.10^-11.N^4 - 2,86.10^-9.N^3 - 1,60.10^-5.N^2 + 1,78.10^-2.N
  int   N  = analogRead(ldrPin);
  float lx = 1.55*pow(10, -11)*pow(N, 4) - 2.86*pow(10, -9)*pow(N, 3) - 1.60*pow(10, -5)*pow(N, 2) + 1.78*pow(10, -2)*N;
  return exp(lx);
}

#ifndef __main__
void setup(void) 
{
  Serial.begin(9600);
}

void loop(void) 
{
  // Affichage de la luminosité
  int light = read_light();
  Serial.println("Light: " + String(light) + "lx");
  
  delay(1000); // Attendre 1 seconde avant la prochaine lecture
}
#endif
