#include <WiFi.h>

const bool DEBUG = true;      // mode verbeux
const bool WIFI  = false;      // utilisation du wifi
const int  INTERRUPT_PIN = 2; // Broche utilisée pour l'interruption

void processSerialData(void) 
{
  String data = WIFI
	? wifi_read()
	: Serial.readStringUntil('\n') ;  // Lit la chaîne de caractères reçue jusqu'au caractère de nouvelle ligne

  // Traite les données
  if ( data.startsWith("LED=") ) {
      // cmde : LED=[ON|OFF]
      String value = data.substring(4);  // Extrait la valeur après le préfixe "LED="
      if ( value == "ON" )
        led_on();
      else if ( value == "OFF" )
        led_off();
      else
        Serial.println("Erreur commande de la LED");  // Envoie une réponse à la Raspberry
      }
    else if ( data.startsWith("SERVO=") ) {
      // cmde : SERVO=position_départ:position_fin
      String value = data.substring(6);  // Extrait la valeur après le préfixe "SERVO="
      int    separatorIndex = value.indexOf(':');  // Trouve l'index du séparateur ":"
      if ( separatorIndex != -1 ) {
        String value1 = data.substring(0, separatorIndex);  // Extrait la première valeur après le préfixe "SERVO="
        String value2 = data.substring(separatorIndex + 1);  // Extrait la deuxième valeur après le séparateur ":"
        
        rotate( value1.toInt(), value2.toInt() );
        }
      else
        Serial.println("Erreur syntaxe SERVO");  // Envoie une réponse à la Raspberry
      }
   else
    Serial.println("Erreur commande");  // Envoie une réponse à la Raspberry
}

#define __main__
void setup(void) 
{
  Serial.begin(9600);
  
  adt7410_init(); // Démarre la communication I2C
  led_init();
  servo_init();
  if ( WIFI )
    wifi_init();
  
  pinMode(INTERRUPT_PIN, INPUT_PULLUP);  // Configure la broche de l'interruption en entrée avec résistance de tirage
  attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), processSerialData, FALLING);  // Attache l'interruption à la broche
}

void loop(void) 
{
  // Lecture des capteurs
  float temperature = read_temperature();
  int humidity      = read_humidity();
  int light         = read_light();
  int pressure      = read_pressure();
  
  // Affichage des valeurs des capteurs
  if ( DEBUG ) {
    Serial.println("Temperature: " + String(temperature) + " °C");
    Serial.println("Humidity: " + String(humidity) + "%");
    Serial.println("Light: " + String(light) + "%");
    Serial.println("Pressure: " + String(pressure) + "%");
    }

  String data = String(temperature) + ";" + String(humidity) + ";" + String(light) + ";" + String(pressure);
  if ( WIFI ) {
    if ( !wifi_write(data) )
      Serial.println("Erreur connexion client wifi");
    }
  else
    // Envoi des valeurs des capteurs via la communication série
    Serial.print(data + "\r\n");
  
  delay(5000); // Attendre 5s avant la prochaine lecture
}
