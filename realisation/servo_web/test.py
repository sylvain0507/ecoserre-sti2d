import serial
import time

run = True

serialPort = serial.Serial("COM12", 9600, timeout=10)
try:
    while True:
        while serialPort.inWaiting():
            print(serialPort.read(1), end='')


        #serialPort = serial.Serial("COM12", 9600, timeout=10)
        data="SERVO=0:180"
        serialPort.write(data.encode() + b'\n')
        serialPort.flush()  # Attend la fin de l'écriture

        #serialPort = serial.Serial("COM12", 9600, timeout=10)
        data="SERVO=180:0"
        serialPort.write(data.encode() + b'\n')
        serialPort.flush()  # Attend la fin de l'écriture

        time.sleep(2)
except KeyboardInterrupt:
    serialPort.close()

