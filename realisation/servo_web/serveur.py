import serial, time


# Configuration du port série
arduino_port = 'COM12'
baud_rate = 9600 #debit


#initialisation du port série
ser = serial.Serial(arduino_port, baud_rate)


# Envoie la valeur sur le port série

position = "1"
ser.write(position.encode())

ser.close()


