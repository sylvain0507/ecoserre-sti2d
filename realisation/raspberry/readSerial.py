#!/usr/bin/env python3
# coding: utf-8

import serial
import socket
import time
import datetime
import csv

def write_data(directory : str, data : list) -> bool:
    """ enregistrement du fichier journal
    @param  directory -- dossier des logs
    @param  data -- données à enregistrer
    @return True si enregistrement OK, False sinon
    """
    file = datetime.date.today().strftime("%Y-%m-%d") + ".csv"
    try:
        with open(directory + "/" + file, "a", encoding='utf8', newline='') as output:
            now = datetime.datetime.now().strftime("%H:%M:%S")
            writer = csv.writer(output, delimiter=';')
            writer.writerow([now] + data)
        return True
    except IOError as e:
        print("Erreur lors de l'écriture du fichier {} : {}".format(file, str(e).encode('utf-8')))
        return False

def read_serial(serialPort : object) -> list:
    """ lecture du port série
    @param  serialPort -- objet PySerial
    @return la liste des données lues si OK, None si erreur
    """
    try:
        data = serialPort.readline().decode().strip()
        serialPort.flushInput()     # Vide le tampon d'entrée
        return data.split(';')
    except serial.SerialException as e:
        print("Erreur d'ouverture du port série {} : {}".format(port, str(e).encode('utf-8')))
        return None
    except serial.SerialTimeoutException as e:
        print("Timeout lors de la lecture du port série {} : {}".format(port, str(e).encode('utf-8')))
        return None

def read_wifi(ip_address : str, port :   int) -> list:
    """ lecture du port série
    @param  ip_address -- @ IP du client
    @param  port -- numéro du port
    @return la liste des données lues si OK, None si erreur
    """
    try:
        # Connexion au serveur socket
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect((ip_address, port))
        data = client_socket.recv(1024).decode().strip()
        client_socket.close()
        return data.split(';')
    except socket.error as e:
        print("Erreur de connexion au serveur : {}".format(str(e).encode('utf-8')))
        return None

if __name__ == "__main__":
    WIFI = False  # Modifier la valeur selon le mode de connexion souhaité
    IP   = "192.168.100.1"
    PORT = 80
    COM  = "COM12"

    try:
        print("Connexion en cours...")
        serialPort = serial.Serial(COM, 9600)
        while True:
            data = read_wifi(IP, PORT) if WIFI else read_serial(serialPort)
            if data is not None:
                print(data)
                if not write_data("./logs", data):
                    print("Erreur écriture fichier log")
    except KeyboardInterrupt:
        serialPort.close()
