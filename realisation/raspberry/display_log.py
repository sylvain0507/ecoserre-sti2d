#!/usr/bin/env python3
# coding: utf-8

import csv
import matplotlib.pyplot as plt

def plot(day : str, label : str, x : list, y : list, directory : str):
    """ enregistre une image
    @param  day -- jour à afficher
    @param  label -- le paramètre à afficher
    @param  x -- coordonnées en abscisse
    @param  y -- coordonnées en ordonnée
    @param  directory -- dossier de sauvegarde de l'image
    @return le chemein complet de l'image sauvegardée
    """
    plot_file = directory + "/"+ day + "_" + label + ".png" if len(directory) else ""

    plt.figure()    # évite la superposition des images
    plt.plot(x, y)
    plt.xlabel("heure")
    plt.xticks(rotation=45)
    plt.ylabel(label)
    plt.title(day)
    if plot_file != "":
        plt.savefig(plot_file)
    #plt.show()
    return plot_file

def read_csv(filename : str) -> tuple:
    try:
        with open(filename, "r", ) as file:
            # String data = String(temperature) + ";" + String(humidity) + ";" + String(light) + ";" + String(pressure);
            times  = []
            temperature = []
            humidity    = []
            light       = []
            pressure    = []

            for row in csv.reader(file, delimiter=';'):
                times.append(row[0])
                temperature.append(row[1])
                humidity.append(row[2])
                light.append(row[3])
                pressure.append(row[4])
            return times, temperature, humidity, light, pressure
    except IndexError:
        print("Erreur format CSV du fichier", filename)
        return None, None, None, None, None
    except IOError:
        print("Erreur lecture fichier", filename)
        return None, None, None, None, None

def display_temperature(file : str, directory : str = ".") -> str:
    times, temperature, _, _, _ = read_csv(directory+"/"+file)
    if times is not None:
        return plot(file.split('.')[0], "Température", times, temperature, directory)
    return ""

def display_humidity(file : str, directory : str = ".") -> str:
    times, _, humidity, _, _ = read_csv(directory+"/"+file)
    if times is not None:
        return plot(file.split('.')[0], "Humidité", times, humidity, directory)
    return ""

def display_light(file : str, directory : str = ".") -> str:
    times, _, _, light, _ = read_csv(directory+"/"+file)
    if times is not None:
        return plot(file.split('.')[0], "Luminosité", times, light, directory)
    return ""

def display_pressure(file : str, directory : str = ".") -> str:
    times, _, _, _, pressure = read_csv(directory+"/"+file)
    if times is not None:
        return plot(file.split('.')[0], "Pression", times, pressure, directory)
    return ""

def display_logs(file : str, directory : str = ".") -> list:
    png_files = []
    for f in [display_temperature, display_humidity, display_light, display_pressure]:
        png_file = f(file, directory)
        if png_file != "":
            png_files.append(png_file)
    return png_files

if __name__ == "__main__":
    display_logs("2023-07-05.csv", "./logs")
