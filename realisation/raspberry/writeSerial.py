#!/usr/bin/env python3
# coding: utf-8

import serial
import socket
import time


def writeWifi(host: str, port: int, data : str) -> bool:
    """ test connexion wifi Arduino -> rasp """
    try:
        # Établissement de la connexion
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((host, port))
        sock.send(data.encode() + b'\n')
        sock.close()
        return True
    except Exception as e:
        print("Erreur lors de la connexion Wi-Fi :", str(e).encode('utf-8'))
        return False

def writeWifi_UT(host: str, port: int):
    """ test connexion wifi Arduino -> rasp """
    # Envoie les commandes à l'Arduino via la connexion Wi-Fi
    for command in ["SERVO=0:180", "SERVO=180:0", "LED=ON", "LED=OFF"]:
        if writeWifi(host, port, command):
            time.sleep(3)

def writeSerial(serialPort : object, data : str) -> bool:
    """ test connexion du port série Arduino -> rasp """
    try:
        print(data)
        serialPort.write(data.encode() + b'\n')
        serialPort.flush()  # Attend la fin de l'écriture
        return True
    except Exception as e:
        print("Erreur lors de la connexion série :", str(e).encode('utf-8'))
        return False

def writeSerial_UT(serialPort : object):
    """ test connexion du port série Arduino -> rasp """
    # Envoie les commandes à l'Arduino via le port série
    for command in ["SERVO=0:180", "SERVO=180:0", "LED=ON", "LED=OFF", "FAN=ON", "FAN=OFF"]:
        if writeSerial(serialPort, command):
            time.sleep(2)


if __name__ == "__main__":
    WIFI = False  # Modifier la valeur selon le mode de connexion souhaité
    IP   = "192.168.100.1"
    PORT = 80
    COM  = "COM12"

    if WIFI:
        writeWifi_UT(IP, PORT)
    else:
        serialPort = serial.Serial(COM, 9600)
        writeSerial_UT(serialPort)
        serialPort.close()
